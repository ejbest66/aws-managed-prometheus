# aws managed prometheus 

Special Thanks to:
<br> - Harshit Gupta
<br> - Mangash Wagle 
<br>
<br>This repository contains sample application which can be used as reference to deploy prometheus-collector agent in ECS (Fargate resource  Compatibility) with it's integration with AWS Prometheus service.
<br>
<br>Several references from different other sources to compile this repository mentioned below.<br> 
<br>
Started with this video<br>
https://www.youtube.com/watch?v=Wk0h3aGtlUo

Here was part of what helped for the searching<br>
https://aws.amazon.com/blogs/opensource/metrics-collection-from-amazon-ecs-using-amazon-managed-service-for-prometheus/

Source link found above - WHERE THIS CODE came from!<br>
https://github.com/aws-samples/prometheus-for-ecs

For more details on how to use this project kindly refer - https://medium.com/

----------------------------------------------------
Follow the following steps -<br>
<br>1. Setup environment<br>
  - ``` source set_env.sh ```<br>
<br>2. Launch Network - VPC, Subnets, SecurityGroup, InternetGateway, RouteTable, Routes<br>
  - ``` source network-setup/networking-setup.sh ```<br>
<br>3. Create ECS cluster with fargate. Name should match with variable **`$ECS_CLUSTER_NAME`** from **`set_env.sh`**. For this check- https://www.youtube.com/watch?v=Wk0h3aGtlUo at time **3:13** to **3:41** <br>
<br>4. Create IAM policies to allow push metrics from ECS prometheus to AWS Managed PrometheusService<br>
  - ``` source iam.sh ```<br>
<br>5. Create ServiceDiscovery Namespace and register your task defnition service into AWS CLOUDMAP.<br>
  - ``` source cloudmap.sh ```<br>
<br>6. Create Workspace for AWS Managed Prometheus.<br>
  - ``` source amp.sh ```<br> 
  - ONLY IF NOT ALREADY CREATED <br>
  - if not running look and examine steps <br> 
  - do manually if need be <br>
    - go to aws prometheus <br>
    - get workspace id and copy <br>
    - update the workspace id the prometheus.yml.template<br>
Double check and triple check or lose hours of time<br>
<br>7. Execute either of 2 below:<br>
  - Go to AWS Systems Manager > Parameter Store in Console (UI)<br>
    - Create a new Parameter of type text **"ECS-ServiceDiscovery-Namespaces"** with value of variable **`$SERVICE_DISCOVERY_NAMESPACE`** from **`set_env.sh`**.
    - Create a new Parameter  of type text **"ECS-Prometheus-Configuration"** with **content of the "prometheus.yaml"** genereted on your local machine at step 5.
  - Alternatively execute  -<br>
    - ``` source set_aws_system_params.sh ```<br>
<br>8. Create Task Definitions for your ECS. In case you wish to modify the same, do the changes to **webappTaskDefinition.json.template**. This is the place where you can replace your application definition and prometheus params like CPU/Memory
  - ``` source task-definitions.sh ```<br>
<br>9. Finally create services for your TaskDefinitions to launch them.<br>
  - ``` source services.sh ```<br>
<br>10. If you have set **ECS_PROMETHEUS_PUBLIC_IP_ENABLED** to **ENABLED**, then visit it's running task details in ECS cluster to get the public IP.
   You shall be able to access prometheus on port **9090** with this on that IP.<br>
<br>
<br>
https://docs.aws.amazon.com/prometheus/latest/userguide/AMP-onboard-query.html<br>
<br> 
We have to see the above link; these explains, how grafana is needed and details are not available without