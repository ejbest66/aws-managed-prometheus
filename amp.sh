##!/bin/bash
# provided 
# https://github.com/aws-samples/prometheus-for-ecs/blob/main/deploy-prometheus/amp.sh
# 
set -x 
WORKSPACE_ID=$(aws amp create-workspace --alias $AWS_PROMETHEUS_SERVICE_WORKSPACE_ALIAS --query "workspaceId" --output text --region $AWS_REGION)

sed -e s/WORKSPACE/$WORKSPACE_ID/g \
< prometheus.yaml.template \
> prometheus.yaml
